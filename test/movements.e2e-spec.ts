import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { MovementsModule } from '../src/movements/movements.module';
import { MovementsValidationDto } from '../src/movements/dtos/movement-validation.dto';
import { InvalidationReason } from 'src/movements/exceptions/invalid-movements-http.exception';

describe('AppController (e2e)', () => {
    let app: INestApplication;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [MovementsModule]
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    describe('/movements/validation (POST)', () => {
        it('should accept a valid body', () => {
            const body: MovementsValidationDto = {
                movements: [
                    {
                        id: 1,
                        date: new Date('2024-06-23'),
                        amount: 100,
                        label: 'Test'
                    }
                ],
                balances: [
                    {
                        date: new Date('2024-06-23'),
                        balance: 100
                    }
                ]
            };

            return request(app.getHttpServer()).post('/movements/validation').send(body).expect(202).expect({
                message: 'Accepted'
            });
        });

        it("should repond it's a tea pot with reasons", () => {
            const body: MovementsValidationDto = {
                movements: [
                    {
                        id: 1,
                        date: new Date('2024-06-23'),
                        amount: 100,
                        label: 'Test'
                    }
                ],
                balances: [
                    {
                        date: new Date('2024-06-23'),
                        balance: 200
                    }
                ]
            };

            return request(app.getHttpServer())
                .post('/movements/validation')
                .send(body)
                .expect(418)
                .expect({
                    message: 'I’m a teapot',
                    reasons: [
                        {
                            code: 'INVALID_BALANCE',
                            balanceDate: new Date('2024-06-23').toISOString(),
                            balanceDiscrepancy: 100
                        }
                    ]
                });
        });

        describe('Client errors', () => {
            it('should reject request without body by HTTP code 400 ', () => {
                return request(app.getHttpServer()).post('/movements/validation').expect(400);
            });

            it('should reject request with empty balances by HTTP code 400', () => {
                const body = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 100,
                            label: 'Test'
                        }
                    ],
                    balances: []
                };

                return request(app.getHttpServer()).post('/movements/validation').send(body).expect(400);
            });

            it('should reject request with movements by HTTP code 400', () => {
                const body = {
                    movements: [],
                    balances: [
                        {
                            date: new Date('2024-06-23'),
                            balance: 100
                        }
                    ]
                };

                return request(app.getHttpServer()).post('/movements/validation').send(body).expect(400);
            });
        });
    });
});
