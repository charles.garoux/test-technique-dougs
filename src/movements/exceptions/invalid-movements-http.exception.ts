import { HttpException } from '@nestjs/common';

export type InvalidationReason =
    | {
          code: 'DUPLICATED_MOVEMENT_ID';
          movementId: number;
      }
    | {
          code: 'INVALID_BALANCE';
          balanceDate: Date;
          /**
           * The discrepancy between the balance and the sum of the movements at the balance date
           */
          balanceDiscrepancy: number;
      };

export class InvalidMovementsHttpException extends HttpException {
    /**
     * @param reasons The reasons about why the movements are invalid
     */
    constructor(reasons: InvalidationReason[]) {
        const preparedReasons = reasons.map((reason) => {
            if (reason.code === 'INVALID_BALANCE') {
                return {
                    ...reason,
                    balanceDate: reason.balanceDate.toISOString()
                };
            }

            return reason;
        });

        super({ message: 'I’m a teapot', reasons: preparedReasons }, 418);
    }
}
