import { Injectable } from '@nestjs/common';
import { MovementsValidationDto } from './dtos/movement-validation.dto';
import { isMovement, Movement } from './schemas/bank-operation.schema';
import { Balance } from './schemas/balance.schema';
import { InvalidationReason } from './exceptions/invalid-movements-http.exception';

@Injectable()
export class MovementsService {
    validateMovements({
        movements,
        balances
    }: MovementsValidationDto): { isValid: true } | { isValid: false; invalidationReasons: InvalidationReason[] } {
        const invalidationReasons: InvalidationReason[] = [];

        this.getDuplicatedMovementsIds(movements).forEach((movementId) => {
            invalidationReasons.push({ code: 'DUPLICATED_MOVEMENT_ID', movementId });
        });

        const timeline = this.mergeToTimeline(movements, balances);
        let sumToDate = 0;
        for (const currentElement of timeline) {
            if (isMovement(currentElement)) {
                sumToDate += currentElement.amount;
            } else {
                if (sumToDate !== currentElement.balance) {
                    invalidationReasons.push({
                        code: 'INVALID_BALANCE',
                        balanceDate: currentElement.date,
                        balanceDiscrepancy: currentElement.balance - sumToDate
                    });
                }
            }
        }

        return invalidationReasons.length === 0 ? { isValid: true } : { isValid: false, invalidationReasons };
    }

    private getDuplicatedMovementsIds(movements: Movement[]): number[] {
        return movements.map((movement) => movement.id).filter((id, index, ids) => ids.indexOf(id) !== index);
    }

    /**
     * Merge movements and balances into a chronological timeline.
     * On a same date, movements are placed before balances.
     */
    mergeToTimeline(movements: Movement[], balances: Balance[]): (Movement | Balance)[] {
        return [...movements, ...balances].sort(this.compareTimelineElements);
    }

    private compareTimelineElements(a: Movement | Balance, b: Movement | Balance): number {
        if (a.date.getTime() === b.date.getTime()) {
            return isMovement(a) ? -1 : 1;
        }
        return a.date.getTime() - b.date.getTime();
    }
}
