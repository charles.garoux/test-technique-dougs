import { z } from 'zod';

export const movementIdSchema = z.number();
export type MovementId = z.infer<typeof movementIdSchema>;

export const movementTypeSchema = z.object({
    id: movementIdSchema,

    date:
        // The input can be a timestamp, a date string or a date object : it must be transformed to a date object
        z.union([z.date(), z.string(), z.number()]).pipe(z.coerce.date()),

    label: z.string(),

    amount: z.number()
});
export type Movement = z.infer<typeof movementTypeSchema>;
export function isMovement(input: unknown): input is Movement {
    return movementTypeSchema.safeParse(input).success;
}
