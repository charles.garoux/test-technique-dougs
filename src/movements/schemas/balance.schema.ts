import { z } from 'zod';

export const balanceSchema = z.object({
    date:
        // The input can be a timestamp, a date string or a date object : it must be transformed to a date object
        z.union([z.date(), z.string(), z.number()]).pipe(z.coerce.date()),

    balance: z.number()
});
export type Balance = z.infer<typeof balanceSchema>;
export function isBalance(input: unknown): input is Balance {
    return balanceSchema.safeParse(input).success;
}
