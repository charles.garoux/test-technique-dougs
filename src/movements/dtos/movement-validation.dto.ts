import { z } from 'zod';
import { movementTypeSchema } from '../schemas/bank-operation.schema';
import { balanceSchema } from '../schemas/balance.schema';

export const movementsValidationDtoSchema = z.object({
    /**
     * The movements to validate
     */
    movements: z.array(movementTypeSchema).nonempty(),
    /**
     * The balances for the validation
     */
    balances: z.array(balanceSchema).nonempty()
});
export type MovementsValidationDto = z.infer<typeof movementsValidationDtoSchema>;
