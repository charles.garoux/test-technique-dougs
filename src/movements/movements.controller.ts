import { BadRequestException, Body, Controller, Post, HttpCode } from '@nestjs/common';
import { movementsValidationDtoSchema } from './dtos/movement-validation.dto';
import { MovementsService } from './movements.service';
import { InvalidMovementsHttpException } from './exceptions/invalid-movements-http.exception';

@Controller('movements')
export class MovementsController {
    constructor(private readonly movementsService: MovementsService) {}

    @Post('validation')
    @HttpCode(202)
    validateMovements(@Body() body: unknown) {
        const dto = movementsValidationDtoSchema.safeParse(body);
        if (!dto.success) throw new BadRequestException(dto.error.errors);

        const validation = this.movementsService.validateMovements(dto.data);

        if (!validation.isValid) throw new InvalidMovementsHttpException(validation.invalidationReasons);

        return { message: 'Accepted' };
    }
}
