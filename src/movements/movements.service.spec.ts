import { Test, TestingModule } from '@nestjs/testing';
import { MovementsService } from './movements.service';
import { MovementsValidationDto } from './dtos/movement-validation.dto';

describe('MovementsService', () => {
    let service: MovementsService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MovementsService]
        }).compile();

        service = module.get<MovementsService>(MovementsService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe('Validation', () => {
        describe('Acceptance', () => {
            it('should validate empty movements and balances', () => {
                const data: MovementsValidationDto = {
                    // @ts-expect-error for testing empty movements
                    movements: [],
                    // @ts-expect-error for testing empty balances
                    balances: []
                };

                expect(service.validateMovements(data)).toEqual({ isValid: true });
            });

            it('should validate a single movement and balance with the same amount and date', () => {
                const data: MovementsValidationDto = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 100,
                            label: 'Test'
                        }
                    ],
                    balances: [
                        {
                            date: new Date('2024-06-23'),
                            balance: 100
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({ isValid: true });
            });

            it('should validate multiple movements and balances with a growing balance', () => {
                const data: MovementsValidationDto = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 100,
                            label: 'Test'
                        },
                        {
                            id: 2,
                            date: new Date('2024-06-24'),
                            amount: 50,
                            label: 'Test'
                        }
                    ],
                    balances: [
                        {
                            date: new Date('2024-06-23'),
                            balance: 100
                        },
                        {
                            date: new Date('2024-06-24'),
                            balance: 150
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({ isValid: true });
            });

            it('should validate multiple balances with a decreasing balance', () => {
                const data: MovementsValidationDto = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 100,
                            label: 'Test'
                        },
                        {
                            id: 2,
                            date: new Date('2024-06-24'),
                            amount: -50,
                            label: 'Test'
                        }
                    ],
                    balances: [
                        {
                            date: new Date('2024-06-23'),
                            balance: 100
                        },
                        {
                            date: new Date('2024-06-24'),
                            balance: 50
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({ isValid: true });
            });

            it('should validate with a final balance amount of 0 and movements after the balance date', () => {
                const data: MovementsValidationDto = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 100,
                            label: 'Test'
                        },
                        {
                            id: 2,
                            date: new Date('2024-06-24'),
                            amount: -100,
                            label: 'Test'
                        },
                        {
                            id: 3,
                            date: new Date('2024-06-25'),
                            amount: 50,
                            label: 'Test'
                        }
                    ],
                    balances: [
                        {
                            date: new Date('2024-06-23'),
                            balance: 100
                        },
                        {
                            date: new Date('2024-06-24'),
                            balance: 0
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({ isValid: true });
            });

            it('should validate with same balances at different dates', () => {
                const data: MovementsValidationDto = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 100,
                            label: 'Test'
                        },
                        {
                            id: 2,
                            date: new Date('2024-06-25'),
                            amount: 50,
                            label: 'Test'
                        }
                    ],
                    balances: [
                        {
                            date: new Date('2024-06-23'),
                            balance: 100
                        },
                        {
                            date: new Date('2024-06-24'),
                            balance: 100
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({ isValid: true });
            });

            it('should validate with complex movements and balances', () => {
                const data: MovementsValidationDto = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 100,
                            label: 'Test'
                        },
                        {
                            id: 2,
                            date: new Date('2024-06-24'),
                            amount: -50,
                            label: 'Test'
                        },
                        {
                            id: 3,
                            date: new Date('2024-06-25'),
                            amount: 50,
                            label: 'Test'
                        },
                        {
                            id: 4,
                            date: new Date('2024-06-27'),
                            amount: 50,
                            label: 'Test'
                        }
                    ],
                    balances: [
                        {
                            date: new Date('2024-06-22'),
                            balance: 0
                        },
                        {
                            date: new Date('2024-06-23'),
                            balance: 100
                        },
                        {
                            date: new Date('2024-06-24'),
                            balance: 50
                        },
                        {
                            date: new Date('2024-06-25'),
                            balance: 100
                        },
                        {
                            date: new Date('2024-06-26'),
                            balance: 100
                        },
                        {
                            date: new Date('2024-06-28'),
                            balance: 150
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({ isValid: true });
            });
        });

        describe('Rejection', () => {
            it('should reject a single movement with a higher amount than the balance', () => {
                const data: MovementsValidationDto = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 100,
                            label: 'Test'
                        }
                    ],
                    balances: [
                        {
                            date: new Date('2024-06-23'),
                            balance: 50
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({
                    isValid: false,
                    invalidationReasons: [
                        {
                            balanceDate: new Date('2024-06-23'),
                            code: 'INVALID_BALANCE',
                            balanceDiscrepancy: -50
                        }
                    ]
                });
            });

            it('should reject a single movement with a lower amount than the balance', () => {
                const data: MovementsValidationDto = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 50,
                            label: 'Test'
                        }
                    ],
                    balances: [
                        {
                            date: new Date('2024-06-23'),
                            balance: 100
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({
                    isValid: false,
                    invalidationReasons: [
                        {
                            balanceDate: new Date('2024-06-23'),
                            code: 'INVALID_BALANCE',
                            balanceDiscrepancy: 50
                        }
                    ]
                });
            });

            it('should reject with duplicated movement ids', () => {
                const data: MovementsValidationDto = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 0,
                            label: 'Test'
                        },
                        {
                            id: 2,
                            date: new Date('2024-06-23'),
                            amount: 0,
                            label: 'Test'
                        },
                        {
                            id: 2,
                            date: new Date('2024-06-24'),
                            amount: 0,
                            label: 'Test'
                        }
                    ],
                    balances: [
                        {
                            date: new Date('2024-06-24'),
                            balance: 0
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({
                    isValid: false,
                    invalidationReasons: [
                        {
                            code: 'DUPLICATED_MOVEMENT_ID',
                            movementId: 2
                        }
                    ]
                });
            });

            it('should reject with duplicated movement ids and invalid balance', () => {
                const data: MovementsValidationDto = {
                    movements: [
                        {
                            id: 1,
                            date: new Date('2024-06-23'),
                            amount: 100,
                            label: 'Test'
                        },
                        {
                            id: 2,
                            date: new Date('2024-06-24'),
                            amount: 50,
                            label: 'Test'
                        },
                        {
                            id: 2,
                            date: new Date('2024-06-24'),
                            amount: 50,
                            label: 'Test'
                        }
                    ],
                    balances: [
                        {
                            date: new Date('2024-06-24'),
                            balance: 150
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({
                    isValid: false,
                    invalidationReasons: [
                        {
                            code: 'DUPLICATED_MOVEMENT_ID',
                            movementId: 2
                        },
                        {
                            balanceDate: new Date('2024-06-24'),
                            code: 'INVALID_BALANCE',
                            balanceDiscrepancy: -50
                        }
                    ]
                });
            });

            it('should reject with non null balance with no movement', () => {
                const data: MovementsValidationDto = {
                    // @ts-expect-error for testing empty movements
                    movements: [],
                    balances: [
                        {
                            date: new Date('2024-06-23'),
                            balance: 100
                        }
                    ]
                };

                expect(service.validateMovements(data)).toEqual({
                    isValid: false,
                    invalidationReasons: [
                        {
                            balanceDate: new Date('2024-06-23'),
                            code: 'INVALID_BALANCE',
                            balanceDiscrepancy: 100
                        }
                    ]
                });
            });
        });
    });

    describe('Timeline', () => {
        it('should merge movements and balances into a timeline ordered by ascending date and type (movement then balance)', () => {
            const movements = [
                {
                    id: 1,
                    date: new Date('2024-06-23'),
                    amount: 100,
                    label: 'Test'
                },
                {
                    id: 2,
                    date: new Date('2024-06-24'),
                    amount: 50,
                    label: 'Test'
                }
            ];
            const balances = [
                {
                    date: new Date('2024-06-23'),
                    balance: 100
                },
                {
                    date: new Date('2024-06-24'),
                    balance: 150
                },
                {
                    date: new Date('2024-06-25'),
                    balance: 150
                }
            ];

            expect(service.mergeToTimeline(movements, balances)).toEqual([
                {
                    id: 1,
                    date: new Date('2024-06-23'),
                    amount: 100,
                    label: 'Test'
                },
                {
                    date: new Date('2024-06-23'),
                    balance: 100
                },
                {
                    id: 2,
                    date: new Date('2024-06-24'),
                    amount: 50,
                    label: 'Test'
                },
                {
                    date: new Date('2024-06-24'),
                    balance: 150
                },
                {
                    date: new Date('2024-06-25'),
                    balance: 150
                }
            ]);
        });

        it('should merge shuffled movements and balances into a timeline ordered by ascending date and type (movement then balance)', () => {
            const movements = [
                {
                    id: 2,
                    date: new Date('2024-06-24'),
                    amount: 50,
                    label: 'Test'
                },
                {
                    id: 1,
                    date: new Date('2024-06-23'),
                    amount: 100,
                    label: 'Test'
                }
            ];
            const balances = [
                {
                    date: new Date('2024-06-25'),
                    balance: 150
                },
                {
                    date: new Date('2024-06-23'),
                    balance: 100
                },
                {
                    date: new Date('2024-06-24'),
                    balance: 150
                }
            ];

            expect(service.mergeToTimeline(movements, balances)).toEqual([
                {
                    id: 1,
                    date: new Date('2024-06-23'),
                    amount: 100,
                    label: 'Test'
                },
                {
                    date: new Date('2024-06-23'),
                    balance: 100
                },
                {
                    id: 2,
                    date: new Date('2024-06-24'),
                    amount: 50,
                    label: 'Test'
                },
                {
                    date: new Date('2024-06-24'),
                    balance: 150
                },
                {
                    date: new Date('2024-06-25'),
                    balance: 150
                }
            ]);
        });
    });
});
