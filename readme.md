# Test technique Dougs

# Présentation

**Description du projet** :
Test technique pour ma candidature comme développeur chez Dougs.

**Objectifs** :

-   Le projet doit pouvoir vérifier l'intégrité de transaction bancaire.

## Fonctionnalités

### Route POST `/movements/validation`

**"Body" de la requête** :

```typescript
type MovementsValidationDto = {
    movements: {
        id: number;
        date: Date;
        label: string;
        amount: number;
    }[];
    balances: {
        date: Date;
        balance: number;
    }[];
};
```

**"Body" de la réponse en code HTTP `202` en cas de validation réussie** :

```typescript
type ResponseDto = {
    message: 'Accepted';
};
```

**"Body" de la réponse en code HTTP `418` en cas de validation échouée** :

```typescript
type ResponseDto = {
    message: 'I’m a teapot';
    reasons: (
        | {
              code: 'DUPLICATED_MOVEMENT_ID';
              movementId: number;
          }
        | {
              code: 'INVALID_BALANCE';
              balanceDate: string;
              balanceDiscrepancy: number;
          }
    )[];
};
```

# Mise en oeuvre

## Installation

Prérequis :

-   `nvm` (Node Version Manager) : https://github.com/nvm-sh/nvm

**Installer Node.js** :

```bash
nvm install
```

**Installer Yarn** :

```
npm install -g yarn
```

**Installer les dépendances** :

```bash
yarn install
```

## Lancer le projet

```bash
yarn start
```

## Tester le projet

**Lancer les tests unitaires** :

```bash
yarn test
```

**Lancer les tests e2e** :

```bash
yarn test:e2e
```

# Auteur

-   **Charles Garoux** - Développeur Web 🌐, Cloud ☁️ et DevOps ♾️
